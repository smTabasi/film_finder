# -*- coding: utf-8 -*-
import re

from scrapy.exceptions import DropItem

from film_finder.utils import TextUtils, NumUtils
from film_finder.items import WikiFilmItem, ManzoomFilmItem,\
    ImdbFilmItem, SourehFilmItem, CicFilmItem, UNIFIED_GENRE_MAP


class DropPipe:
    def process_item(self, item, spider):
        if 'title' not in item:
            raise DropItem(f'Missing TITLE!')
        elif 'lang' in item and 'فارسی' not in item['lang']:
            raise DropItem(f'Invalid LANG in {item["title"]}')
        return item


class UnifyTitlePipe:
    def process_item(self, item, spider):

        if 'فهرست' in item['title']:
            for word in ['فیلم', 'سریال', 'مجموعه', 'تلویزیون']:
                if word in item['title']:
                    raise DropItem(f'Invalid TITLE in {item}')

        item['title'] = re.sub(r'\([^)]*\)', '', item['title']).strip()
        if item['title'] == '':
            raise DropItem(f'Empty TITLE in {item}')
        else:
            return item


class SummarizeCastPipe:
    def process_item(self, item, spider):
        if 'actors' in item and type(item['actors']) is str :
            actors = item['actors'].split('؛ ')
            item['actors'] = '، '.join(actors)

        return item

class ConvertNumbersPipe:
    def process_item(self, item, spider):
        if isinstance(item, WikiFilmItem):
            for field in WikiFilmItem.NUMERIC_FIELDS:
                if field in item and type(item[field]) is not int:
                    item[field] = NumUtils.to_int(item[field], min_digits=WikiFilmItem.NUMERIC_FIELDS[field], return_min=False)
        return item

class ConvertTimeLengthPipe:
    def process_item(self, item, spider):
        if 'length' not in item or item['length'] is None:
            return item


        if isinstance(item, WikiFilmItem):          # processed in spider
            item['length'] = int(item['length'])

        elif isinstance(item, CicFilmItem):       # PT1H26M14S
            len_value = item['length'].replace('PT', '')
            len_value_list = TextUtils.multi_split(len_value, 'HMS')
            hours = int(len_value_list[0]) if 'H' in len_value else 0
            minutes = int(len_value_list[1]) if 'M' in len_value else 0
            item['length'] = hours * 60 + minutes

        elif isinstance(item, ManzoomFilmItem):
            item['length'] = NumUtils.to_int(item['length'], min_digits=2)

        elif isinstance(item, SourehFilmItem):
            item['length'] = int(item['length'])

        return item

class CleanFieldsPipe:
    def process_item(self, item, spider):

        for field in item:
            if type(item[field]) is str:
                str_value = item[field].replace(',', '،')
                str_value = TextUtils.remove_bracket(str_value)
                str_value = NumUtils.to_english_num(str_value)
                str_value = TextUtils.normalize_persian(str_value)
                str_value = TextUtils.strip_special_chars(str_value, chars='"؛')
                item[field] = str_value

        return item


class UnifyGenrePipe:
    def process_item(self, item, spider):
        if 'genre' not in item or item['genre'] is None:
            return item

        if isinstance(item, WikiFilmItem):
            item_genre_list = TextUtils.multi_split(item['genre'], list('-_' + '،؛') + ['\sو\s'] + ['\.'] + ['\/'])
            item_genre_list += item['genre'].split(' ')
            item_genre_list = list(filter(None, item_genre_list))
            item_genre_list = list(filter(len, item_genre_list))
            item_genre_list = list(map(str.strip, item_genre_list))
            item_genre_list = list(map(lambda s: TextUtils.remove_para(s, keep_inside=True), item_genre_list))
        else:
            item_genre_list = item['genre'].split('، ')

        unified_genre_list = []

        for genre in item_genre_list:
            unified_genre = UNIFIED_GENRE_MAP.get(genre) or None
            if unified_genre is None:
                continue
            if type(unified_genre) is list:
                unified_genre_list += unified_genre
            else:
                unified_genre_list.append(unified_genre)

        item['genre'] = ' '.join(set(unified_genre_list))

        return item