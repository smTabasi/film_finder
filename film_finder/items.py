# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class WikiFilmItem(scrapy.Item):
    NUMERIC_FIELDS = {
        'year': 4,
        'seasons': 1,
        'episodes': 1,
        'length': 2
    }

    film_id = scrapy.Field()
    title = scrapy.Field()
    is_movie = scrapy.Field()
    genre = scrapy.Field()
    year = scrapy.Field()

    length = scrapy.Field()

    director = scrapy.Field()
    writer = scrapy.Field()
    producer = scrapy.Field()

    # special fields
    channel = scrapy.Field()
    seasons = scrapy.Field()
    episodes = scrapy.Field()

    publisher = scrapy.Field()
    singer = scrapy.Field()
    composer = scrapy.Field()
    actors = scrapy.Field()

    lang = scrapy.Field()
    box_office = scrapy.Field()


class ManzoomFilmItem(scrapy.Item):
    NUMERIC_FIELDS = {
        'year': 4,
        'film_id': 7,
        'rate_count': 1,
        'followers': 1
    }
    GENRES = ['آموزشی', 'تاریخی', 'ترسناک', 'تله تئاتر', 'تله فیلم', 'جنایی', 'جنگی', 'حادثه‌ای', 'خارجی', 'خانوادگی',
              'خبری', 'درام', 'دفاع مقدس', 'رئالیتی-شو', 'زندگی‌نامه‌ای', 'عاشقانه', 'عروسکی', 'عروسکی نمایشی',
              'علمی تخیلی', 'فانتزی', 'فیلم کوتاه', 'ماجراجویی', 'مخصوص بزرگسالان', 'مذهبی', 'مسابقه', 'مستند',
              'معمایی', 'مناسبتی', 'مهیج', 'موزیکال', 'نوار', 'هیجانی', 'ورزشی', 'وسترن', 'پویانمایی', 'کمدی',
              'کودک و نوجوان', 'گفت و گو محور']
    film_id = scrapy.Field()
    src_id = scrapy.Field()
    title = scrapy.Field()
    is_movie = scrapy.Field()
    year = scrapy.Field()
    length = scrapy.Field()
    genre = scrapy.Field()

    director = scrapy.Field()
    actors = scrapy.Field()

    # special fields
    manzoom_story = scrapy.Field()
    actor_roles = scrapy.Field()

    rate = scrapy.Field()
    rate_count = scrapy.Field()
    followers = scrapy.Field()
    fine_rates = scrapy.Field()

    manzoom_poster = scrapy.Field()
    audio = scrapy.Field()

    short_infos = scrapy.Field()


class ImdbFilmItem(scrapy.Item):
    NUMERIC_FIELDS = {
        'year': 4,
        'film_id': 7,
        'rate_count': 1,
        'followers': 1
    }
    GENRES = ['Drama', 'Crime']
    film_id = scrapy.Field()

    title = scrapy.Field()
    is_movie = scrapy.Field()
    year = scrapy.Field()
    length = scrapy.Field()
    genre = scrapy.Field()

    director = scrapy.Field()
    actors = scrapy.Field()

    # special fields
    rate = scrapy.Field()

    rate_count = scrapy.Field()
    short_infos = scrapy.Field()


class SourehFilmItem(scrapy.Item):
    film_id = scrapy.Field()

    title = scrapy.Field()
    is_movie = scrapy.Field()
    year = scrapy.Field()
    genre = scrapy.Field()
    length = scrapy.Field()

    director = scrapy.Field()
    writer = scrapy.Field()
    actors = scrapy.Field()

    soureh_story = scrapy.Field()


class CicFilmItem(scrapy.Item):
    NUMERIC_FIELDS = {
        'year': 4,
        'film_id': 7,
        'rate_count': 1,
        'followers': 1
    }
    film_id = scrapy.Field()
    is_movie = True
    title = scrapy.Field()
    year = scrapy.Field()
    length = scrapy.Field()
    genre = scrapy.Field()

    director = scrapy.Field()
    writer = scrapy.Field()
    actors = scrapy.Field()

    # special fields
    eng_title = scrapy.Field()
    fing_title = scrapy.Field()
    cic_poster = scrapy.Field()
    cic_story = scrapy.Field()


UNIFIED_GENRE_MAP = {
    # manzoom
    'آموزشی': 'education',
    'تاریخی': 'history',  # also in cic, wiki, soureh
    'ترسناک': 'horror',  # also in cic
    'تله تئاتر': 'teleplay',
    'تله فیلم': 'telefilm',
    'جنایی': 'crime',  # also in cic, wiki
    'جنگی': 'war',  # also in cic, wiki, soureh
    'حادثه‌ای': 'action',  # also in cic
    'خارجی': 'foreign',
    'خانوادگی': 'family',  # also in cic, wiki, soureh
    'خبری': 'news',
    'درام': 'drama',  # also in cic, wiki, soureh
    'دفاع مقدس': ['war', 'war/iraq-sacred-defence'],  # also in wiki
    'رئالیتی-شو': 'reality show',
    'زندگی‌نامه‌ای': 'biography',
    'عاشقانه': 'romance',  # also in cic, wiki
    'عروسکی': 'puppet',
    'عروسکی نمایشی': ['puppet', 'puppet/show'],  # also in wiki
    'علمی تخیلی': 'sci-fi',     # also in soureh
    'فانتزی': 'fantasy',  # also in cic, , soureh
    'فیلم کوتاه': 'short',
    'ماجراجویی': 'adventure',
    'مخصوص بزرگسالان': 'adult',
    'مذهبی': 'religious',  # also in wiki, soureh
    'مسابقه': 'competition',
    'مستند': 'documentary', # also in soureh
    'معمایی': 'mystery',  # also in wiki
    'مناسبتی': 'occasion',
    'مهیج': 'thriller',
    'موزیکال': 'musical',
    'نوار': 'cassette',
    'هیجانی': 'thriller',  # also in wiki
    'ورزشی': 'sport',  # also in cic
    'وسترن': 'western',     # also in soureh
    'پویانمایی': 'animation',
    'کمدی': 'comedy',  # also in cic, wiki
    'کودک و نوجوان': 'children',
    'گفت و گو محور': 'conversation',
    # cic
    'موسیقایی': 'musical',
    'هیجان‌انگیز': 'thriller',
    'ماجراجویانه': 'adventure',
    'اسرارآمیز': 'mystery',     # also in soureh
    'فیلم سیاه': 'dark',  # new genre
    'موسیقی': 'musical',
    'علمی- تخیلی': 'sci-fi',
    'زندگی‌نامه': 'biography',  # also in wiki
    'کودک': 'children',  # also in wiki

    # wiki
    'اجتماعی': 'social',  # new genre, also in soureh
    'پلیسی': 'action',      # also in soureh
    'طنز آیتمی': 'comedy',
    'نمایش تلویزیونی': 'tv-show',  # new genre
    'فیلم اکشن': 'action',
    'طنز تاریخی': ['comedy', 'history'],
    'ماورائی': 'fantasy',
    'درام خانوادگی': ['drama', 'family'],
    'تاریخ معاصر ایران': 'history',
    'اجتماعی خانوادگی': ['social', 'family'],
    'کودکان': 'children',
    'عاطفی': 'romance',
    'تخیلی': 'fantasy',     # also in soureh
    'سیاسی': 'political',  # new genre, also in soureh
    'انقلابی': ['revolution', 'political'],  # new genre
    'دینی': 'religious',
    'پزشکی': 'medical',  # new genre
    'خانواده': 'family',
    'طنز اجتماعی': ['social', 'comedy'],
    'تراژدی': 'tragedy',  # new genre
    'جاسوسی': ['spy', 'action', 'thriller'],  # new genre
    'رمانتیک': 'romance',
    'اجتماعی طنز': ['social', 'comedy'],
    'امنیتی': ['security', 'political'],  # new genre
    'انرژی هسته‌ای': ['nuclear', 'political'],  # new genre
    'سریال تلویزیونی': 'tv-show',
    'داستانی': 'fiction',  # new genre
    'عروسک‌گردانی': 'puppet',
    'طنزآمیز': 'comedy',
    'ماورایی': 'fantasy',
    'فراطبیعی': 'fantasy',
    'کمدی انتقادی': ['social', 'comedy'],
    'مجموعه تلویزیونی': 'tv-show',
    'ملودرام': 'melodrama',  # new genre
    'ملودرام اجتماعی': ['melodrama', 'social'],
    'نوجوان': 'children',
    'ماورايي': 'fantasy',
    'اکشن': 'action',
    'انتقادی': 'social',
    'طنز': 'comedy',

    # soureh
    'كارتونی': 'animation',
    'فلسفی': 'philosophy',
    'حادثه ای': 'action',
    'كمدی': 'comedy',
    'كودكان': 'children',
    'وحشت': 'horror',
    'عشقی': 'romance',
    'رئال انیمیشن': 'animation',
    'جنائی': 'crime',
}
