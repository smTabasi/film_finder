import re


class NumUtils:
    AR_NUMBERS = '١٢٣٤٥٦٧٨٩٠'
    FA_NUMBERS = '۱۲۳۴۵۶۷۸۹۰'
    EN_NUMBERS = '1234567890'

    @staticmethod
    def to_english_num(mixed_str):

        ar2en = str.maketrans(NumUtils.AR_NUMBERS, NumUtils.EN_NUMBERS)
        fa2en = str.maketrans(NumUtils.FA_NUMBERS, NumUtils.EN_NUMBERS)

        return mixed_str.translate(ar2en).translate(fa2en)

    @staticmethod
    def to_int(mixed_str, min_digits=2, return_min=True):

        eng_num_only = NumUtils.to_english_num(mixed_str)
        eng_num_only = eng_num_only.replace('٬', '').replace(',', '')       # 20,000,000
        numbers = re.findall(r'[0-9]+', eng_num_only)
        numbers = list(filter(lambda n: len(n) >= min_digits, numbers))
        integers = list(map(int, numbers))

        if len(integers) == 0:
            return -1    # todo change this

        if return_min:
            return min(integers)
        else:
            return max(integers)

    @staticmethod
    def to_persian_num(num):

        en2fa = str.maketrans(NumUtils.EN_NUMBERS, NumUtils.FA_NUMBERS)
        ar2fa = str.maketrans(NumUtils.AR_NUMBERS, NumUtils.FA_NUMBERS)

        return str(num).translate(en2fa).translate(ar2fa)


class TextUtils:
    AR_YEHS = 'ى' + 'ي'
    FA_YEHS = 'ی' + 'ی'


    @staticmethod
    def multi_split(text, seps):

        full_seps = [fr'\s*{sep}\s*' for sep in list(seps)]
        return re.split(r'|'.join(full_seps), text)

    @staticmethod
    def remove_para(text, keep_inside=False):
        if keep_inside:
            return re.sub(r'[()]', '', text).strip()
        else:
            return re.sub(r'\([^)]*\)', '', text).strip()

    @staticmethod
    def remove_bracket(text, keep_inside=False):
        if keep_inside:
            return re.sub(r'[\[\]]', '', text).strip()
        else:
            return re.sub(r'\[[^\]]*\]', '', text).strip()

    @staticmethod
    def strip_special_chars(text, chars='؛'):
        striped = re.sub(fr'^[{chars}\s]+', '', text)
        return re.sub(fr'[{chars}\s]+$', '', striped)

    @staticmethod
    def edit_distance(s1, s2):
        if len(s1) < len(s2):
            return TextUtils.edit_distance(s2, s1)

        # len(s1) >= len(s2)
        if len(s2) == 0:
            return len(s1)

        previous_row = range(len(s2) + 1)
        for i, c1 in enumerate(s1):
            current_row = [i + 1]
            for j, c2 in enumerate(s2):
                insertions = previous_row[j + 1] + 1  # j+1 instead of j since previous_row and current_row are one character longer
                deletions = current_row[j] + 1  # than s2
                substitutions = previous_row[j] + (c1 != c2)
                current_row.append(min(insertions, deletions, substitutions))
            previous_row = current_row

        return previous_row[-1]

    @staticmethod
    def is_similar(s1, s2, threshold=0.9):
        edit_dist = TextUtils.edit_distance(s1, s2)
        length = len(s1 + s2)
        similarity = (length - edit_dist) / length
        return similarity > threshold

    @staticmethod
    def normalize_persian(text):
        ar2fa_yeh = str.maketrans(TextUtils.AR_YEHS, TextUtils.FA_YEHS)

        new_text = text.translate(ar2fa_yeh)
        new_text = new_text.replace('  ', ' ').strip()
        new_text = new_text.replace(u'\u200f', '')
        new_text = new_text.replace(u'\u200c', '')

        return new_text

    @staticmethod
    def split_noalpha(s):
        return re.split(r'[^a-zA-Zا-ی]+', s)


class SynonymRepository:
    synonyms = {}

