import datetime
import json
import re
from w3lib.html import remove_tags

import scrapy
from scrapy import signals
from pydispatch import dispatcher
import pandas as pd

from film_finder.items import SourehFilmItem
from film_finder.utils import NumUtils, TextUtils

from scrapy.utils.log import configure_logging
import logging


class SourehSpider(scrapy.Spider):
    name = 'soureh'

    FILM_BY_YEAR_CHAR_URL = 'http://www.sourehcinema.com/Years/TitleByYear.aspx?Year=%s&Start=%s'
    FILM_CREW_URL = 'http://www.sourehcinema.com/Title/FullCast_Crew.aspx?Id=%s'

    PERSIAN_CHARS = list('اآبپتثجچحخدذرزژسشصضطظعغفقكگلمنوهي') + ['Others']

    DIRECTOR = 'كارگردان :'
    WRITER = 'نويسنده :'
    ACTORS = 'گروه بازيگران'
    MOVIE = 'فیلم سینمایی'

    custom_settings = {
        'FEEDS': {
            'data/soureh_info.csv': {
                'format': 'csv',
                'encoding': 'utf-8',
                'store_empty': False,
                'fields': ['title', 'film_id', 'year', 'genre', 'length', 'director', 'writer', 'actors'],
            },
            'data/soureh_info.json': {
                'format': 'json',
                'encoding': 'utf-8',
                'store_empty': False
            }
        },
        # 'ITEM_PIPELINES': {  ####################################
        #     'film_finder.pipelines.DropPipe': 100,
        #     'film_finder.pipelines.UnifyTitlePipe': 200,
        #     'film_finder.pipelines.ConvertNumbersPipe': 300,
        #     'film_finder.pipelines.SummarizeCastPipe': 400,
        #     'film_finder.pipelines.ConvertTimeLengthPipe': 500,
        #     'film_finder.pipelines.CleanFieldsPipe': 600,
        # }

    }

    def __init__(self, range='1309,1400', **kwargs):
        super().__init__(**kwargs)
        self.range = range.split(',')
        self.processed = 0

    def start_requests(self):

        if len(self.range) >= 2:
            start, end = self.range[:2]
        else:
            start, end = self.range[0], self.range[0] + 1

        for year in range(int(start), int(end)):
            for start_ch in self.PERSIAN_CHARS:
                yield scrapy.Request(url=self.FILM_BY_YEAR_CHAR_URL % (str(year), start_ch),
                                     callback=self.parse_film_list, meta={'year': year})
            print('>>>>>>>>>> ' + str(year))

    def parse_film_list(self, response):

        film_links = response.css('span#Hp a::attr(href)').getall()

        for link in film_links:
            yield scrapy.Request(url=link, callback=self.parse_film_info,
                                 meta={'year': response.meta['year'], 'film_id': re.findall(r'[0-9]{4,}', link)[0]})

    def parse_film_info(self, response):

        title = response.css('table#Table9 font::text').get().replace('"', '').strip()

        crew = response.css('table#Table10 ::text').getall()
        crew = list(map(str.strip, crew))
        crew = list(filter(len, crew))

        director, writer = '', ''
        for i, cr in enumerate(crew):
            if cr == self.DIRECTOR:
                director = crew[i + 1].replace('  ', ' ')
            elif cr == self.WRITER:
                writer = crew[i + 1].replace('  ', ' ')

        genre = response.css('table#Table3 font.Tb::text').get()
        genre = TextUtils.strip_special_chars(genre, '/') if genre else None
        story = response.css('table#Table11 td.Tb::text').getall()
        if len(story) >= 2:
            story = TextUtils.strip_special_chars(story[1].strip(), '"').replace('\r\n', ' ')
        else:
            story = None

        length = -1

        extra_info = response.css('table.tb_t td font::text').getall()
        for info in extra_info:
            if 'دقيقه' in info:
                length = NumUtils.to_int(info, min_digits=2)

        film_item = SourehFilmItem(film_id=response.meta['film_id'], title=title,
                                   year=response.meta['year'], genre=genre, soureh_story=story,
                                   director=director, writer=writer, length=length)

        self.processed += 1
        self.log_collection_size()

        yield scrapy.Request(self.FILM_CREW_URL % response.meta['film_id'], callback=self.parse_film_crew,
                             meta={'film_item': film_item})

    def parse_film_crew(self, response):

        film_item = response.meta['film_item']

        crew_tables = response.css('table table table table')

        for table in crew_tables:
            if self.ACTORS in ' '.join(table.css('td::text').getall()):
                actor_list = table.css('a.Titr::text').getall()
                break
        else:
            yield film_item
            return

        actor_list = list(map(str.strip, actor_list))
        actor_list = list(filter(len, actor_list))
        film_item['actors'] = '، '.join(actor_list)

        yield film_item

    def log_collection_size(self):
        if self.processed % 100 == 0:
            logging.warning(f'>> {self.processed} films collected!')
