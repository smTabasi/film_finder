import datetime
import json
import re
from w3lib.html import remove_tags

import scrapy
from scrapy import signals
from pydispatch import dispatcher
import pandas as pd

from film_finder.items import CicFilmItem
from film_finder.utils import NumUtils, TextUtils

from scrapy.utils.log import configure_logging
import logging


class CicSpider(scrapy.Spider):
    name = 'cic'

    CIC_LIST_PAGE_URL = 'https://cicinema.com/fa/movies?per_page=24&category=داستانی+بلند&page=%d&sort=date&order=desc'
    DEFAULT_IMAGE = 'https://static.cicinema.com/storage/styles/poster_large/default-poster.jpg'

    custom_settings = {
        'FEEDS': {
            'data/cic_info.csv': {
                'format': 'csv',
                'encoding': 'utf-8',
                'store_empty': False,
                'fields': ['title', 'film_id', 'eng_title', 'fing_title',
                           'year', 'genre', 'length', 'director', 'writer', 'actors'],
            },
            'data/cic_info.json': {
                'format': 'json',
                'encoding': 'utf-8',
                'store_empty': False
            }
        }
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.processed = 0

    def start_requests(self):

        for page in range(1, 120):
            yield scrapy.Request(url=self.CIC_LIST_PAGE_URL % page,
                                 callback=self.parse_cic_list)
            print('>>>>>>>>>>>>> ' + str(page))

    def parse_cic_list(self, response):

        film_links = response.css('div.movie-item-style-2 h6>a::attr(href)').getall()

        for link in film_links:
            yield scrapy.Request(url=link, callback=self.parse_cic_info,
                                 meta={'film_id': re.findall(r'[0-9]+', link)[0]})

    def parse_cic_info(self, response):

        info_list = response.css('div#overview')

        titles = info_list.css('p[itemprop=alternateName]::text').getall() + ['', '']
        if len(titles) >= 3:
            title, eng_title, fing_title = titles[:3]
        else:
            return

        director = info_list.css('a[itemprop=director]>span::text').get()
        writer = info_list.css('a[itemprop=author]>span::text').getall()
        writer = '، '.join(writer)
        actors = info_list.css('a[itemprop=actor]>span::text').getall()
        actors = '، '.join(actors)

        genre = info_list.css('meta[itemprop=genre]::attr(content)').getall()
        genre = '، '.join(genre)
        year = int(info_list.css('meta[itemprop=datePublished]::attr(content)').get())
        length = info_list.css('meta[itemprop=duration]::attr(content)').get()

        story = info_list.css('p[itemprop=description]::text').get()
        poster = response.css('div.movie-img>img::attr(src)').get()
        if poster == self.DEFAULT_IMAGE:
            poster = None

        film_item = CicFilmItem(film_id=response.meta['film_id'],
                                title=title, eng_title=eng_title, fing_title=fing_title,
                                director=director, writer=writer, actors=actors,
                                year=year, genre=genre, length=length,
                                cic_story=story, cic_poster=poster)

        self.processed += 1

        self.log_collection_size()
        yield film_item

    def log_collection_size(self):
        if self.processed % 50 == 0:
            logging.warning(f'>> {self.processed} films collected!')
