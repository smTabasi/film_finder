import datetime
import json
import re
from w3lib.html import remove_tags

import scrapy
from scrapy import signals
from pydispatch import dispatcher
import pandas as pd

from film_finder.items import ManzoomFilmItem
from film_finder.utils import NumUtils, TextUtils

from scrapy.utils.log import configure_logging
import logging


class ManzoomSpider(scrapy.Spider):

    name = 'manzoom'

    MANZ_QUERY_URL = 'https://www.manzoom.ir/search/?q=%s&t=1'
    MANZ_CREW_URL = 'https://www.manzoom.ir/crew/%s'

    DIRECTOR = 'کارگردان:'
    ACTORS = 'بازیگران:'
    MOVIE = 'فیلم'

    custom_settings = {
        'FEEDS': {
            'data/manzoom_info.csv': {
                'format': 'csv',
                'encoding': 'utf-8',
                'store_empty': False,
                'fields': ['title', 'film_id', 'is_movie', 'genre', 'year', 'length',
                           'director', 'actors', 'rate', 'rate_count', 'followers', 'src_id'],
            },
            'data/manzoom_info.json': {
                'format': 'json',
                'encoding': 'utf-8',
                'store_empty': False
            }
        }
    }

    def __init__(self, src='wiki,soureh,cic'):
        self.processed = 0
        self.source = src
        self.film_ids = set()

    def start_requests(self):

        for src in self.source.split(','):
            src_films = pd.read_csv(f'data/{src}_info.csv')
            for i, src_film in src_films.iterrows():

                title = src_film['title']
                meta_data = {
                    'title': title,
                    'src_id': f"{src}/{src_film['film_id']}",
                    'year': src_film['year'],
                    'is_movie': src_film.get('is_movie') or True,
                    'director': src_film['director'],
                }

                query_url = self.MANZ_QUERY_URL % title
                yield scrapy.Request(url=query_url, callback=self.parse_search_results, meta=meta_data)

                self.processed += 1

    def parse_search_results(self, response):

        src_film = response.meta

        film_candidates = response.css('div.right-part>div.main-box a.item')
        exact_match = False

        for cand_film in film_candidates:
            res_title = cand_film.css(' ::text').get().strip()
            link = cand_film.attrib['href']
            if res_title == src_film['title']:
                exact_match = True
                yield scrapy.Request(url=link, callback=self.parse_film_info, meta=src_film)
            elif exact_match == False and TextUtils.is_similar(res_title, src_film['title'], threshold=0.6):
                # print(f'================================== SIMILAR ============={src_film["title"]}========{res_title}============')
                yield scrapy.Request(url=link, callback=self.parse_film_info, meta=src_film)

    def parse_film_info(self, response):

        title_year = ' '.join(response.css('div.info-top-header ::text').getall())
        title = re.findall(r'^[^\(\)]+', title_year)
        if len(title) > 0:
            title = title[0].strip()
        else:
            return

        year = NumUtils.to_int(title_year, min_digits=4, return_min=True)

        staff = response.css('div.underline-links.maznoom-d div')
        director, actors = '', []
        for tag in staff:
            if self.DIRECTOR in tag.css(' ::text').get():
                director = tag.css('a::text').get().strip()
            elif self.ACTORS in tag.css(' ::text').get():
                actors = tag.css('a::text').getall()
            elif 'مشاهده لیست' in tag.css(' ::text').get():
                film_id = tag.css('a::attr(href)').re('tt[0-9]+')[0]
                if film_id in self.film_ids:
                    return

        film_item = ManzoomFilmItem(title=title, film_id=film_id, src_id=response.meta['src_id'],
                                    year=year, director=director)

        film_item['actors'] = actors
        film_item['manzoom_poster'] = response.css('div#main-page-part div.right-col img::attr(src)').get()
        film_item['audio'] = response.css('audio>source::attr(src)').get()

        infos_for_story = response.css('div.left-col>div::text').getall()
        for i, text in enumerate(infos_for_story):
            if 'خلاصه' in text:
                film_item['manzoom_story'] = TextUtils.strip_special_chars(infos_for_story[i+1], '"')
                break

        film_item['short_infos'] = []
        for tag in response.css('div.short-info'):
            value = tag.css(' ::text').get().strip()
            film_item['short_infos'].append(value)
            if 'دقیقه' in value:
                film_item['length'] = value
            else:
                for genre in ManzoomFilmItem.GENRES:
                    if genre in value:
                        film_item['genre'] = value

        film_item['is_movie'] = self.MOVIE in ' '.join(film_item['short_infos'])

        if not self.film_match(film_item, response.meta):
            return

        rate = response.css('div.movie-circle-rate::text').get()

        if rate is not None:
            film_item['rate'] = float(rate)
            film_item['rate_count'] = int(response.css('span.rate-count::text').get())
            film_item['followers'] = int(response.css('a.followers-link span::text').get())

            fine_rates = response.css('div.result-item')
            film_item['fine_rates'] = {}
            for tag in fine_rates:
                name = tag.css('span.keyword::text').get()
                value = float(tag.css('span.result-rate-float::text').get())
                film_item['fine_rates'][name] = value

        self.film_ids.add(film_item['film_id'])
        self.log_collection_size()

        yield scrapy.Request(self.MANZ_CREW_URL%film_id, callback=self.parse_crew, meta={'film_item': film_item})

    def parse_crew(self, response):

        film_item = response.meta['film_item']

        all_crew_tags = response.css('div.slide-container>div')
        actors_tags = None

        for i, tag in enumerate(all_crew_tags):
            header_title = tag.css('div.title::text').get()
            if header_title is not None and 'بازیگران' in header_title:

                actors_tags = all_crew_tags[i+1].css('div.maznoom-g')
                break
        else:
            yield film_item
            return

        actor_roles = {}
        for tag in actors_tags:
            actor = tag.css('::text').get()
            actor = TextUtils.strip_special_chars(actor, chars='"')
            role = tag.css('p.maznoom-e::text').get()
            actor_roles[actor] = role

        film_item['actor_roles'] = actor_roles

        yield film_item

    def film_match(self, manz_film, src_film):

        # year_match = abs(manz_film['year'] - int(src_film['year'])) <= 3 or (src_film['year'] == -1)
        title_match = TextUtils.is_similar(manz_film['title'], src_film['title'], threshold=0.7)
        if 'director' in src_film and 'director' in manz_film:
            director_match = TextUtils.is_similar(manz_film['director'], src_film['director'], threshold=0.7)
        else:
            return False

        return title_match and director_match

    def log_collection_size(self):
        if len(self.film_ids) % 50 == 0:
            logging.warning(f'>> {len(self.film_ids)} films matched from {self.processed}!')
