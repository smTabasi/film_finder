import datetime
import json
import re
import scrapy
from w3lib.html import remove_tags
from scrapy import signals
from pydispatch import dispatcher
from film_finder.items import WikiFilmItem

from film_finder.utils import NumUtils, TextUtils

from scrapy.utils.log import configure_logging
import logging


class WikiSpider(scrapy.Spider):
    name = 'wiki'

    WIKI_URL = 'https://fa.wikipedia.org%s'
    SHOWS_URL = WIKI_URL % '/wiki/فهرست_مجموعه%E2%80%8Cهای_تلویزیونی_ایران'
    MOVIES_YEAR_URL = WIKI_URL % '/wiki/فهرست_فیلم%E2%80%8Cهای_ایرانی'
    MOVIES_PAGES_URL = WIKI_URL % '/wiki/رده:فیلم‌ها به زبان فارسی'

    NO_PAGE = '(صفحه وجود ندارد)'
    FILM_URL = WIKI_URL % '/wiki/' + '%s_(فیلم)'
    FILM_YEAR_URL = WIKI_URL % '/wiki/' + '%s_(فیلم_%s)'

    FULL_JSON_PATH = 'wiki_infobox_full.json'

    FIELDS_MAP = {
        'genre': ['گونه', 'ژانر'],
        'year': ['انتشار اولیه', 'تاریخ (های) انتشار', 'تاریخ انتشار', 'تاریخ‌های انتشار', 'اولین نمایش در',
                 'تاریخ نشر'],
        'channel': ['شبکهٔ اصلی'],
        'seasons': ['تعداد فصل‌ها', 'فصل‌ها'],
        'episodes': ['تعداد قسمت‌ها', 'قسمت‌ها'],
        'length': ['مدت', 'مدت زمان'],
        'director': ['کارگردان'],
        'writer': ['نویسنده', 'نویسندگان', 'نویسنده‌ها'],
        'actors': ['بازیگران'],
        'producer': ['تهیه‌کننده'],
        'singer': ['آهنگ پایان'],
        'lang': ['زبان', 'زبان‌های اصلی', 'زبان‌(های) اصلی'],
        'composer': ['آهنگساز', 'موسیقی'],
        'box_office': ['گیشه', 'فروش گیشه'],
        'publisher': ['ناشر', 'توزیع‌کننده'],
    }

    custom_settings = {
        'DOWNLOAD_DELAY': 0.05,
        'FEEDS': {
            'data/wiki_info.csv': {
                'format': 'csv',
                'encoding': 'utf-8',
                'store_empty': False,
                'fields': ['title', 'film_id', 'is_movie', 'genre', 'lang', 'year', 'channel', 'seasons', 'episodes',
                           'length',
                           'box_office', 'director', 'writer', 'producer', 'publisher', 'singer', 'composer',
                           'actors'],
            },
            'data/wiki_info.json': {
                'format': 'json',
                'encoding': 'utf-8',
                'store_empty': False
            }
        }
    }

    def __init__(self):
        dispatcher.connect(self.spider_closed, scrapy.signals.spider_closed)
        self.film_list = []

    def start_requests(self):
        yield scrapy.Request(url=self.SHOWS_URL, callback=self.parse_shows)
        # yield scrapy.Request(url=self.MOVIES_YEAR_URL, callback=self.parse_movie_year)
        yield scrapy.Request(url=self.MOVIES_PAGES_URL, callback=self.parse_movies_page)

    def parse_shows(self, response):
        title_tags = response.css("div#mw-content-text>div>ul li")
        for tag in title_tags:
            link = tag.css('a')
            if len(link) == 0:
                continue
            text = ''.join(tag.css(' ::text').getall())
            year = NumUtils.to_int(text, min_digits=4)

            if self.NO_PAGE in link.attrib['title']:
                title = link.css('::text').get()
                title = TextUtils.remove_para(title)
                film_item = WikiFilmItem(title=title, year=year)
                self.film_list.append({'عنوان': title, 'تاریخ انتشار': year})
            else:
                title_url = self.WIKI_URL % link.attrib['href']
                yield scrapy.Request(url=title_url, callback=self.parse_infobox,
                                     meta={'year': year, 'try_new_url': False, 'use_year': False, 'is_movie': False})

    def parse_movies_page(self, response):

        movie_links = response.css('div#mw-pages ul>li>a::attr(href)').getall()

        for href in movie_links:
            yield scrapy.Request(url=self.WIKI_URL % href, callback=self.parse_infobox,
                                 meta={'year': None, 'try_new_url': False, 'use_year': False, 'is_movie': True})

        next_link = response.css('div#mw-pages>a')
        for link in next_link:
            if 'بعدی' in link.css('::text').get():
                href = link.css('::attr(href)').get()
                yield scrapy.Request(url=self.WIKI_URL%href, callback=self.parse_movies_page)
                break

    def parse_movie_year(self, response):
        year_tags = response.css("div#mw-content-text>div>ul li a")
        for tag in year_tags:
            name = tag.attrib['title']
            print('\n\n>>>>>>>>>>>>>>>>>>>>>>> ', name)
            if 'فهرست' in name and self.NO_PAGE not in name:
                year_url = self.WIKI_URL % tag.attrib['href']
                yield scrapy.Request(url=year_url, callback=self.parse_movie_list)

    def parse_movie_list(self, response):

        page_title = response.css('h1#firstHeading::text').get()
        year = NumUtils.to_int(page_title, min_digits=4)

        movie_tags = response.css("div#mw-content-text>div>ul li")

        for tag in movie_tags:
            title = tag.css(' ::text').getall()[0]
            link_texts = tag.css('a ::text').getall()
            links = tag.css('a::attr(href)').getall()

            if len(links) > 0 and link_texts[0] == title:
                url = self.WIKI_URL % links[0]
            else:
                url = self.WIKI_URL % '/wiki/' + title.strip()

            yield scrapy.Request(url=url, callback=self.parse_infobox,
                                 meta={'year': year, 'try_new_url': True, 'use_year': False, 'is_movie': True})

        if len(movie_tags) == 0:
            self.parse_movie_table(response)

    def parse_movie_table(self, response):

        page_title = response.css('h1#firstHeading::text').get()
        year = NumUtils.to_int(page_title, min_digits=4)

        movie_tags = response.css("div#mw-content-text table.wikitable tr")

        for tag in movie_tags:
            hrefs = tag.css('td a::attr(href)').getall()
            if len(hrefs) > 0:
                yield scrapy.Request(url=self.WIKI_URL % hrefs[0], callback=self.parse_infobox,
                                     meta={'year': year, 'try_new_url': True, 'use_year': False, 'is_movie': True})

    def parse_infobox(self, response):

        film_id = re.findall('[^/]+$', response.url)[0]
        title = response.css("h1#firstHeading i::text").get()
        if title is None:
            return
        title = TextUtils.remove_para(title, keep_inside=False).strip()

        film_item = WikiFilmItem(film_id=film_id, title=title, is_movie=response.meta['is_movie'])
        if response.meta['year'] is not None:
            film_item['year'] = response.meta['year']
        else:
            year = response.css('div#mw-content-text>div>p')
            year = ' '.join(year[0].css(' ::text').getall())
            film_item['year'] = NumUtils.to_int(remove_tags(year), min_digits=2, return_min=False)
        infobox_rows = response.css("table.infobox tr")

        infobox_dict = {'عنوان': title}
        for row in infobox_rows:
            name = row.css('th::text').get()
            value = row.css('td ::text').getall()
            if name and value:
                value = list(filter(lambda s: len(s) > 1, value))
                infobox_dict[name] = '، '.join(value)

        if not self.is_valid_infobox(infobox_dict):
            self.try_new_url(title.strip(), response.meta)
            return

        self.film_list.append(infobox_dict)
        self.log_collection_size()

        for field, names in self.FIELDS_MAP.items():
            for name in set(names).intersection(infobox_dict.keys()):
                if field in film_item:
                    film_item[field] = str(film_item[field]) + ' / ' + str(infobox_dict[name])
                else:
                    film_item[field] = infobox_dict[name]
            # if field == 'year':
            # print('>>>>>> ', set(infobox_dict.keys()))
        yield film_item

    def is_valid_infobox(self, infobox_dict):
        for lang_key in self.FIELDS_MAP['lang']:
            if lang_key in infobox_dict and 'فارسی' not in infobox_dict[lang_key]:
                return False
        for name in self.FIELDS_MAP['director']:
            if name in infobox_dict:
                return True
        return False

    def try_new_url(self, title, meta):
        new_meta = meta
        if not meta['try_new_url']:
            return
        if meta['use_year']:
            new_url = self.FILM_YEAR_URL % (title, NumUtils.to_persian_num(meta['year']))
            new_meta['use_year'] = False
            new_meta['try_new_url'] = False
            yield scrapy.Request(new_url, callback=self.parse_infobox, meta=new_meta)
        else:
            new_url = self.FILM_URL % title
            new_meta['use_year'] = True
            new_meta['try_new_url'] = True
            yield scrapy.Request(new_url, callback=self.parse_infobox, meta=new_meta)

    def spider_closed(self):
        self.save_full_json()

    def save_full_json(self):
        with open(self.FULL_JSON_PATH, 'w', encoding='utf-8') as export_file:
            json.dump(self.film_list, export_file, ensure_ascii=False)

    def log_collection_size(self):
        if len(self.film_list) % 100 == 0:
            logging.warning(f'>> {len(self.film_list)} films collected!')
