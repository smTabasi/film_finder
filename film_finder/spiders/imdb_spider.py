import datetime
import json
import re
from w3lib.html import remove_tags

import scrapy
from scrapy import signals
from pydispatch import dispatcher
import pandas as pd

from film_finder.items import ManzoomFilmItem
from film_finder.utils import NumUtils, TextUtils

from scrapy.utils.log import configure_logging
import logging


class ImdbSpider(scrapy.Spider):

    name = 'imdb'

    IMDB_MAIN_URL = 'https://www.imdb.com/%s'
    IRAN_FILM_LIST = 'https://www.imdb.com/search/title/?country_of_origin=ir&sort=year,desc&start=1001&view=simple'
    PERSIAN_FILM_LIST = 'https://www.imdb.com/search/title/?title_type=feature&primary_language=fa&sort=year,desc&start=1001&view=simple'

    DIRECTOR = 'کارگردان:'
    ACTORS = 'بازیگران:'
    MOVIE = 'فیلم سینمایی'

    custom_settings = {
        'FEEDS': {
            'imdb_info.csv': {
                'format': 'csv',
                'encoding': 'utf-8',
                'store_empty': False,
                'fields': ['title', 'film_id', 'is_movie', 'genre', 'year', 'length',
                           'director', 'actors', 'rate', 'rate_count'],
            },
            'imdb_info.json': {
                'format': 'json',
                'encoding': 'utf-8',
                'store_empty': False
            }
        }
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.processed = 0
        self.film_ids = set()

    def start_requests(self):

        # yield scrapy.Request(url=self.IRAN_FILM_LIST, callback=self.parse_imdb_list)
        yield scrapy.Request(url=self.PERSIAN_FILM_LIST, callback=self.parse_imdb_list)

    def parse_imdb_list(self, response):

        film_links = response.css('div.col-title a::attr(href)')
        exact_match = False

        for cand_film in film_candidates:
            res_title = cand_film.css(' ::text').get().strip()
            link = cand_film.attrib['href']
            if res_title == wiki_info['title']:
                exact_match = True

        next_page_link = response.css('div.desc a::attr(href)').get()
        yield scrapy.Request(url=self.IMDB_MAIN_URL%next_page_link, callback=self.parse_imdb_list)


    def parse_film_info(self, response):

        title_year = ' '.join(response.css('div.info-top-header ::text').getall())
        title = re.findall(r'^[^\(\)]+', title_year)
        if len(title) > 0:
            title = title[0].strip()
        else:
            return

        year = NumUtils.to_int(title_year, min_digits=4, return_min=True)

        staff = response.css('div.underline-links.maznoom-d div')
        director, actors = '', []
        for tag in staff:
            if self.DIRECTOR in tag.css(' ::text').get():
                director = tag.css('a::text').get().strip()
            elif self.ACTORS in tag.css(' ::text').get():
                actors = tag.css('a::text').getall()
            elif 'مشاهده لیست' in tag.css(' ::text').get():
                film_id = tag.css('a::attr(href)').re('tt[0-9]+')[0]
                if film_id in self.film_ids:
                    return

        film_item = ManzoomFilmItem(title=title, film_id=film_id, year=year, director=director)

        film_item['actors'] = actors

        film_item['short_infos'] = []
        for tag in response.css('div.short-info'):
            value = tag.css(' ::text').get().strip()
            film_item['short_infos'].append(value)
            if 'دقیقه' in value:
                film_item['length'] = value
            else:
                for g in ManzoomFilmItem.GENRES:
                    if g in value:
                        film_item['genre'] = value
                        break

        film_item['is_movie'] = self.MOVIE in film_item['short_infos']

        if not self.film_match(film_item, response.meta):
            return

        film_item['rate'] = float(response.css('div.movie-circle-rate::text').get())
        film_item['rate_count'] = int(response.css('span.rate-count::text').get())
        film_item['followers'] = int(response.css('a.followers-link span::text').get())

        fine_rates = response.css('div.result-item')
        film_item['fine_rates'] = {}
        for tag in fine_rates:
            name = tag.css('span.keyword::text').get()
            value = float(tag.css('span.result-rate-float::text').get())
            film_item['fine_rates'][name] = value

        self.film_ids.add(film_item['film_id'])
        self.log_collection_size()
        yield film_item

    def film_match(self, manz_film, wiki_film):

        year_match = abs(manz_film['year'] - wiki_film['year']) <= 3 or (wiki_film['year'] == -1)
        title_match = TextUtils.is_similar(manz_film['title'], wiki_film['title'], threshold=0.7)
        director_match = TextUtils.is_similar(manz_film['director'], wiki_film['director'], threshold=0.7)

        # print('>>>>>>>>>>>>', manz_film['year'], wiki_film['year'], year_match)
        # print('>>>>>>>>>>>>', manz_film['title'], wiki_film['title'], title_match)
        # print('>>>>>>>>>>>>', manz_film['director'], wiki_film['director'], director_match)

        return title_match and director_match

    def log_collection_size(self):
        if len(self.film_ids) % 50 == 0:######################################################
            logging.warning(f'>> {len(self.film_ids)} films matched from {self.processed}!')
