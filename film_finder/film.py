import logging
import math
import tqdm

import pandas as pd
import numpy as np
from copy import deepcopy

from film_finder.utils import TextUtils, NumUtils
from film_finder.items import WikiFilmItem, SourehFilmItem, CicFilmItem, ManzoomFilmItem
from typing import Union, Dict, List, Iterable


class FilmRepository:

    def __init__(self):
        self.all_films: Dict[Film] = {}
        self.key_fields = ['is_movie', 'title', 'director', 'year']
        # self.scoring_fields = ['year', 'rate', 'rate_count', 'followers']

        self.groups: Dict = {}
        self.group_weights: Dict = {}
        self.group_baselines: Dict = {}

        self.merged_films = []

    @staticmethod
    def from_dataframe(df):
        repo = FilmRepository()
        repo.add_dataframe(df)
        return repo

    def add_dataframe(self, df):

        dfna = df.fillna({'year': 0,
                          'length': 0,
                          'rate_count': 0,
                          'followers': 0,
                          'seasons': 0,
                          'episodes': 0,
                          'rate': 0.0,
                          'director': '',
                          'writer': '',
                          'producer': '',
                          'publisher': '',
                          'singer': '',
                          'composer': '',
                          'actors': '',
                          'genre': '',
                          'is_movie': True})

        for i, item in tqdm.tqdm(dfna.iterrows()):
            self.add_film(dict(item))

    def add_film(self, film):
        key = Film.from_item(film, keys=self.key_fields)
        if key in self.all_films:
            self.all_films[key].add_extra_item(film)
            self.merged_films.append((self.all_films[key], Film.from_item(film)))
        else:
            self.all_films[key] = Film.from_item(film)

    def drop_numerics(self, fields, bounds):
        """
        bounds must be list of tuples: [(min, max), ...]
        items are kept if field value equals to bounds.
        """
        key_films = deepcopy(list(self.all_films.keys()))
        drop_count = 0

        for key in key_films:
            film = self.all_films[key]
            for field, bound in zip(fields, bounds):
                if film[field] < bound[0] or film[field] > bound[1]:
                    del self.all_films[key]
                    drop_count += 1
                    break
        if drop_count > 0:
            logging.info(f'>> {drop_count} items dropped for out of bounds {fields}')

    def drop_empty_str(self, fields):
        key_films = deepcopy(list(self.all_films.keys()))
        drop_count = 0

        for key in key_films:
            film = self.all_films[key]
            for field in fields:
                if type(film[field]) is str and film[field].strip() == '':
                    del self.all_films[key]
                    drop_count += 1
                    break
        if drop_count > 0:
            logging.info(f'>> {drop_count} items dropped for empty {fields}')

    def drop_if_field_in(self, field, values):
        key_films = deepcopy(list(self.all_films.keys()))
        drop_count = 0

        for key in key_films:
            film = self.all_films[key]
            if film[field] in values:
                del self.all_films[key]
                drop_count += 1
        if drop_count > 0:
            logging.info(f'>> {drop_count} items dropped for {values} {field}s')

    def drop_if_field_contains(self, field, value):
        key_films = deepcopy(list(self.all_films.keys()))
        drop_count = 0

        for key in key_films:
            film = self.all_films[key]
            if value in film[field]:
                del self.all_films[key]
                drop_count += 1
        if drop_count > 0:
            logging.info(f'>> {drop_count} items dropped for {field} containing {value}')

    def group_by(self, key_fields: Iterable[str], filter_key_values: Dict):
        self.groups: Dict = {}
        self.group_weights: Dict = {}

        for kf, film in self.all_films.items():

            matches_filter = True

            for field, value in filter_key_values.items():
                if film.get(field) != value:
                    matches_filter = False

            if not matches_filter:  # skip films that mismatch filter
                continue

            group_key = {}
            for field in key_fields:
                group_key[field] = str(film.get(field))
                if field == 'genre':
                    continue

            group_keys = []

            if 'genre' in key_fields:  # multiple genre films are duplicated into multiple groups
                genres = film.get('genre').split(', ')
                for genre in genres:
                    duplicate_key = deepcopy(group_key)
                    duplicate_key['genre'] = genre
                    group_keys.append(duplicate_key)
            else:
                group_keys.append(group_key)

            for gk_dict in group_keys:
                group_key = frozenset(gk_dict.items())
                if group_key in self.groups:
                    self.groups[group_key].append(deepcopy(film))
                    self.group_weights[group_key] += 1
                else:
                    self.groups[group_key] = [deepcopy(film)]
                    self.group_weights[group_key] = 1

    def calculate_group_baselines(self, fields=['year', 'rate', 'rate_count', 'followers']):

        for key, group in self.groups.items():
            records = [item.get_dict(fields) for item in group]
            group_df = pd.DataFrame.from_records(records)
            baselines = {}
            for field in fields:
                baselines[(field, 'min')] = 0.0 if math.isnan(group_df[field].min(skipna=True)) else group_df[
                    field].min(skipna=True)
                baselines[(field, 'max')] = 1.0 if math.isnan(group_df[field].max(skipna=True)) else group_df[
                    field].max(skipna=True)
                baselines[(field, 'avg')] = 0.0 if math.isnan(group_df[field].mean(skipna=True)) else group_df[
                    field].mean(skipna=True)
                baselines[(field, 'std')] = 1.0 if math.isnan(group_df[field].std(skipna=True)) else group_df[
                    field].std(skipna=True)
            self.group_baselines[key] = baselines
            for film in group:
                film.set_baselines(baselines)

    def sort_groups(self, scoring_weights=None, genre_weights=None):
        for key, group in self.groups.items():

            for film in group:
                if scoring_weights:
                    film.set_scoring_weights(scoring_weights)
                if genre_weights:
                    film.set_genre_weights(genre_weights)

            group.sort(reverse=True)

    def get_group_df(self, kv: Dict, fields: List):
        group = self.groups[frozenset(kv.items())]

        records = [film.get_dict(fields) for film in group]
        return pd.DataFrame.from_records(records)

    def get_group_weights(self):
        weight_list = []
        for group_kv, weight in self.group_weights.items():
            group_kvw = dict(group_kv)
            group_kvw['weight'] = weight
            weight_list.append(group_kvw)
        return weight_list

    def set_group_weights(self, group_weights, zero_others=True):
        """
        group_weights:  List of dicts, each containing 'weight' key-value plus other key-values of group.
                        All keys & values must be str, except weight value!
        """
        new_weights = deepcopy(group_weights)
        for gw in new_weights:
            weight = gw.pop('weight')
            self.group_weights[frozenset(gw.items())] = weight

        new_keys = [frozenset(gw.items()) for gw in new_weights]
        if zero_others:
            for k in self.group_weights.keys():
                if k not in new_keys:
                    self.group_weights[k] = 0

    def get_best_df(self, n, fields: List):
        """
        supposes that group_by(), calculate_group_baselines(), sort_groups() are called before.
        """

        best_films = []
        best_film_set = set()
        all_groups = deepcopy(self.groups)

        group_keys, group_probs = list(zip(*self.group_weights.items()))
        group_probs = [weight / sum(group_probs) for weight in group_probs]
        group_choices = np.random.choice(len(group_keys), 10 * n, replace=True, p=list(group_probs))

        i = 0

        available_groups = {group_key: 1 for group_key, group_prob in zip(group_keys, group_probs) if group_prob > 0.0}

        while len(best_films) < n:
            if i >= len(group_choices):
                logging.info('>> out of samples!')
                break
            group_key = group_keys[group_choices[i]]
            i += 1
            if len(all_groups[group_key]) > 0:
                best_in_group = all_groups[group_key].pop(0)
                if best_in_group not in best_film_set:
                    best_film_set.add(best_in_group)
                    best_films.append(best_in_group.get_dict(fields))
            else:
                available_groups[group_key] = 0
                if sum(available_groups.values()) == 0:
                    logging.info('>> all groups are empty!')
                    break

        return pd.DataFrame.from_records(best_films)

    def __contains__(self, item):
        return film in self.all_films

    def __len__(self):
        return len(self.all_films)

    def to_dataframe(self, fields):
        records = [film.get_dict(fields) for key, film in self.all_films.items()]
        return pd.DataFrame.from_records(records)


class Film:

    def __init__(self):
        self.fields = {}
        self.baselines = {}
        self.scoring_weights = {'year': 1.0, 'rate': 1.0, 'rate_count': 0.75, 'followers': 0.25}
        self.genre_weights = {}

    @staticmethod
    def from_item(initial_item, keys=None):
        film = Film()
        for key, value in initial_item.items():
            if keys is not None and key not in keys:
                continue
            film.fields[key] = Film.unify_value(key, value)

        # TODO: define default values for non-existing key/cols

        if 'is_movie' not in film:
            film.fields['is_movie'] = True

        return film

    def add_extra_item(self, extra_item):

        new_item = Film.from_item(extra_item)

        if len(self.fields) == 0:
            self.fields = deepcopy(new_item.fields)
        elif not self.can_add(new_item):
            return False

        for key, value in extra_item.items():
            unified_value = self.unify_value(key, value)
            if key in self.fields:
                self.fields[key] = self.solve_conflict(key, unified_value)
            else:
                self.fields[key] = unified_value

        return True

    def can_add(self, film_item):
        return self == film_item

    @staticmethod
    def unify_value(key, value):

        new_value = value

        ## TODO: unify all values here
        if type(new_value) is str:
            new_value = TextUtils.normalize_persian(new_value)

        if key in ['year', 'length', 'rate_count', 'followers', 'seasons', 'episodes']:
            return 0 if math.isnan(new_value) else int(new_value)

        elif key == 'film_id':
            return str(new_value)

        elif key in ['rate']:
            return 0.0 if math.isnan(new_value) else float(new_value)

        elif key in ['director', 'writer', 'producer', 'publisher', 'singer', 'composer']:
            return new_value

        elif key in ['actors']:
            if type(new_value) is list:
                return '، '.join(new_value)

        elif key == 'genre':
            genres = new_value.replace('war/iraq-sacred-defence', 'defence')
            genres = genres.replace('sci-fi', 'scifi')
            genres = genres.replace('puppet/show', 'puppet')
            genres = genres.replace('tv show', 'tvshow')
            genres = genres.replace('tv-show', 'tvshow')
            genres = genres.replace('reality show', 'realityshow')
            genres = set(TextUtils.split_noalpha(genres))
            genres = list(map(str.strip, genres))
            genres = filter(len, genres)
            return ', '.join(genres)

        return new_value

    def solve_conflict(self, key, extra_value):
        if key == 'year':
            return min(self[key], int(extra_value))

        elif key in ['length', 'rate', 'rate_count', 'followers', 'seasons',
                     'episodes']:  # some of them are specific to one source
            return max(self[key], int(extra_value))

        elif key == 'title':
            return self[key] if len(self[key]) < len(extra_value) else extra_value

        elif key == 'film_id':
            return self[key] + ', ' + extra_value

        elif key in ['writer', 'producer', 'publisher', 'singer', 'composer', 'actors']:
            all_crew = self[key] + '، ' + extra_value
            all_crew = set(all_crew.split('، '))
            all_crew = list(map(str.strip, all_crew))
            all_crew = list(map(lambda s: s.replace('  ', ' '), all_crew))
            all_crew = filter(len, all_crew)
            return '، '.join(all_crew)

        elif key == 'genre':
            all_genres = self['genre'] + ', ' + extra_value
            all_genres = set(all_genres.split(', '))
            all_genres = list(map(str.strip, all_genres))
            all_genres = filter(len, all_genres)
            return ', '.join(all_genres)

        return self[key] or extra_value

    def set_baselines(self, baseline_dict):
        self.baselines = baseline_dict

    def set_scoring_weights(self, scoring_weights):
        self.scoring_weights = scoring_weights

    def set_genre_weights(self, genre_weights):
        self.genre_weights = genre_weights

    def calculate_score(self, method='min_max', replace_null='min'):
        scores = []

        for field, weight in self.scoring_weights.items():
            if replace_null == 'min':
                default_value = self.baselines.get((field, 'min'), 0.0)
            else:
                default_value = self.baselines.get((field, 'avg'), 0.0)
            value = float(self.fields.get(field, default_value))

            if method == 'min_max':
                if self.baselines.get((field, 'max'), 1.0) == self.baselines.get((field, 'min'), 0.0):
                    value = 0
                else:
                    value = value - self.baselines.get((field, 'min'), 0.0)
                    value = value / (self.baselines.get((field, 'max'), 1.0) - self.baselines.get((field, 'min'), 0.0))
            elif method == 'std':
                value = value - self.baselines.get((field, 'avg'), 0.0)
                value = value / self.baselines.get((field, 'std'), 1.0)

            scores.append(value * weight)

        for genre, weight in self.genre_weights.items():
            if genre in self['genre']:
                scores.append(weight)

        self.fields['score'] = sum(scores)

        return self.fields['score']

    def get_dict(self, fields):
        result = {}
        for field in fields:
            result[field] = self.fields.get(field)
        return result

    def get(self, key):
        return self.fields.get(key)

    def __eq__(self, other):

        if 'is_movie' in self and 'is_movie' in other:
            if self['is_movie'] != other['is_movie']:
                return False

        if 'title' in self and 'title' in other:
            if not TextUtils.is_similar(self['title'], other['title'], threshold=0.8):
                return False
            if NumUtils.to_int(self['title']) != NumUtils.to_int(other['title']):
                # multiple versions of a film, paytakht 1, 2,...
                return False
        else:
            logging.warning('>> Missing Title!!')

        if 'director' in self and 'director' in other and len(self['director'] + other['director']) > 0:
            if not TextUtils.is_similar(self['director'], other['director'], threshold=0.7):
                return False
        # else:
        #     logging.warning('>> Missing Director!!')

        if 'year' in self and 'year' in other:
            if abs(self['year'] - other['year']) >= 3:
                return False

        return True

    def __contains__(self, field):
        return field in self.fields

    def __getitem__(self, item):
        return self.fields[item]

    def __setitem__(self, key, value):
        self.fields[key] = value

    def __cmp__(self, other):
        if self.calculate_score() == other.calculate_score():
            return 0
        if self.calculate_score() > other.calculate_score():
            return 1
        return -1

    def __lt__(self, other):
        return self.calculate_score() < other.calculate_score()

    def __le__(self, other):
        return self.calculate_score() <= other.calculate_score()

    def __gt__(self, other):
        return self.calculate_score() > other.calculate_score()

    def __ge__(self, other):
        return self.calculate_score() >= other.calculate_score()

    def __hash__(self):
        return hash(self['director'].split(' ')[-1])  # hash based on director family name

    def __str__(self):
        return str(self.fields)

    def __repr__(self):
        return str(self.fields)


if __name__ == '__main__':
    manz = pd.read_csv('C:/Users\MohsenTbs/PycharmProjects/film_finder/data/manzoom_info.csv')
    wiki = pd.read_csv('C:/Users\MohsenTbs/PycharmProjects/film_finder/data/wiki_info.csv')
    soureh = pd.read_csv('C:/Users\MohsenTbs/PycharmProjects/film_finder/data/soureh_info.csv')
    cic = pd.read_csv('C:/Users\MohsenTbs/PycharmProjects/film_finder/data/cic_info.csv')

    repo = FilmRepository.from_dataframe(manz)

    # repo.add_dataframe(wiki)
    # repo.add_dataframe(soureh)
    # repo.add_dataframe(cic)

    repo.drop_empty_str(['genre', 'director'])
    repo.drop_numerics(['year'], [(1375, 1400)])
    repo.group_by(['genre', 'is_movie'], {'is_movie': False})

    # repo.group_by(['is_movie', 'genre'])
    # repo.calculate_group_baselines()
    # repo.sort_groups()
    #
    # gdf = repo.get_group_df({'genre':'drama', 'is_movie':'False'}, ['title', 'score'])
    # g = repo.groups[frozenset({'genre': 'drama', 'is_movie': 'False'}.items())]
